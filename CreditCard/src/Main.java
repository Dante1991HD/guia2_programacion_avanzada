import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		//se instancia la clase tarjeta
		Tarjeta persona1;
		persona1 = new Tarjeta("Dante", 0);
        System.out.println("El usuario: " + persona1.getNombre() + " tiene un saldo de: " + persona1.getSaldo());
        //se instancian los metodos de actualización de saldo y de mostrar saldo en pantalla
        //se realiza un abono a la tarjeta
		persona1.actualizar_saldo(1000, true);
		persona1.muestrar_saldo();
		//se realiza cargo a la tarjeta
		persona1.actualizar_saldo(500, false);
		persona1.muestrar_saldo();
	}
}
