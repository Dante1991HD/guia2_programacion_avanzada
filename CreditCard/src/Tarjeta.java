
public class Tarjeta {
	private String nombre;
	private int saldo;
	//constructor
	Tarjeta(String nombre, int saldo){
		this.setNombre(nombre);
		this.setSaldo(saldo);
	}
	//metodo para actualizar saldo
	void actualizar_saldo(int monto, boolean movimiento) {
		if(movimiento) {
			this.saldo += monto;
			}
		else {
			this.saldo -= monto;
			}
	}
	//metodo para mostrar el saldo
	void muestrar_saldo() {
        System.out.println("El usuario: " + this.nombre + " luego de la transacción tiene un saldo de: " + this.saldo);
	}
	
	//setters y getter para las variables
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getSaldo() {
		return saldo;
	}
	
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
}
