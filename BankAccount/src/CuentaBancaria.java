
public class CuentaBancaria {
	private String nombreCliente;
	private int numeroCuenta;
	private int saldo;
	
	
	CuentaBancaria (String nombre, int nCuenta, int saldo){
	this.setNombreCliente(nombre);
	this.setNumeroCuenta(nCuenta);
	this.setSaldo(saldo);
	}
	//Se crean los setters y getters de los atributos de clase cuenta bancaría
	public String getNombreCliente() {
		return nombreCliente;
	}
	
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	
	public int getSaldo() {
		return saldo;
	}
	
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	
	public int getNumeroCuenta() {
		return numeroCuenta;
	}
	
	public void setNumeroCuenta(int numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	// se define el metodo ingreso como la actualización del saldo y regresa true
	boolean ingreso(int monto) {
		this.saldo += monto;
		return true;
	}
	// se degine el metodo giro, este se lleva a cabo solo si el saldo es superior a 0, de lo contrario regresa false
	boolean giro(int monto) {
		if (saldo - monto >= 0) {
			this.saldo -= monto;
			return true;
		}
		return false;
	}
	//se genera el metodo transferencia y se reutiliza la lógica del giro
	boolean transferencia(CuentaBancaria cuenta, int monto) {
		System.out.println(cuenta.getSaldo());
		if(cuenta.giro(monto)) {
			this.saldo += monto;
			return true;
		}
		return false;
		
	}

		
}
