
public class Main {

	public static void main(String[] args) {
		// se instancia la clase Cuenta bancaria
		CuentaBancaria cuenta1 = new CuentaBancaria("Dante", 123, 100);
		//se prueba y se utiliza getter
		System.out.println("El usuario " + cuenta1.getNombreCliente() + " cuenta número " + cuenta1.getNumeroCuenta() + " tiene un saldo de: " + cuenta1.getSaldo());
		CuentaBancaria cuenta2 = new CuentaBancaria("Dante", 124, 100);
		//se prueba el metodo ingreso
		cuenta1.ingreso(50);
		System.out.println(cuenta1.getSaldo());
		//se prueba el metodo giro
		cuenta1.ingreso(50);
		System.out.println(cuenta1.getSaldo());
		//se prueba el metodo transferencia
		cuenta1.transferencia(cuenta2, 100);
		System.out.println(cuenta1.getSaldo());
		System.out.println(cuenta2.getSaldo());


	}
}

